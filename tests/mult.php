<?
// compose and print html code that produces multiplication table.

for($i = 0; $i < 10; $i++) {
	echo "<div>";

	/* for loop */
	for($y = 0; $y < 10; $y++) {
		print $i . " * " . $y . " = " . ($i * $y ) . "<br>";
	}

	echo "</div>";
}
