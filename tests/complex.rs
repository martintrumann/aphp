use std::fs::read_to_string;

fn cmp_files(a: &str, b: &str) {
    let str = read_to_string(a).unwrap();

    let out = aphp::compile_str(&str).unwrap();

    let php = read_to_string(b).unwrap();

    assert_eq!(out, php)
}

#[test]
fn mult() {
    cmp_files("tests/mult.aphp", "tests/mult.php");

    cmp_files("tests/mult.php", "tests/mult.php");
}

#[test]
fn class() {
    cmp_files("tests/class.aphp", "tests/class.php");

    cmp_files("tests/class.php", "tests/class.php");
}
