#[test]
fn simple() {
    let code = r#"<?let world = "world"; echo "hello " . world;"#;

    let out = aphp::compile_str(code).unwrap();

    assert_eq!(out, r#"<?$world = "world"; echo "hello " . $world;"#)
}

#[test]
fn more_complex() {
    let code = r#"<?
let out = "here is a more complex example.";

echo out;
"#;

    let out = r#"<?
$out = "here is a more complex example.";

echo $out;
"#;

    assert_eq!(aphp::compile_str(code).unwrap(), out);
}

#[test]
fn array() {
    let php = r#"<?
$arr = ['lastname', 'email', 'phone'];
var_dump(implode(",", $arr)); // string(20) "lastname,email,phone"
"#;

    let inp = r#"<?
let arr = ['lastname', 'email', 'phone'];
var_dump(implode(",", arr)); // string(20) "lastname,email,phone"
"#;

    assert_eq!(aphp::compile_str(inp).unwrap(), php);
}

#[test]
fn a_lot() {
    let php = r#"<?
$findme    = 'a';
$mystring1 = 'xyz';
$mystring2 = 'ABC';

$pos1 = stripos($mystring1, $findme);
$pos2 = stripos($mystring2, $findme);

// Nope, 'a' is certainly not in 'xyz'
if ($pos1 === false) {
    echo "The string '$findme' was not found in the string '$mystring1'";
}

// Note our use of ===.  Simply == would not work as expected
// because the position of 'a' is the 0th (first) character.
if ($pos2 !== false) {
    echo "We found '$findme' in '$mystring2' at position $pos2";
}
"#;

    let inp = r#"<?
let findme    = 'a';
let mystring1 = 'xyz';
let mystring2 = 'ABC';

let pos1 = stripos(mystring1, findme);
let pos2 = stripos(mystring2, findme);

// Nope, 'a' is certainly not in 'xyz'
if (pos1 === false) {
    echo "The string '$findme' was not found in the string '$mystring1'";
}

// Note our use of ===.  Simply == would not work as expected
// because the position of 'a' is the 0th (first) character.
if (pos2 !== false) {
    echo "We found '$findme' in '$mystring2' at position $pos2";
}
"#;

    assert_eq!(aphp::compile_str(inp).unwrap(), php);
    assert_eq!(aphp::compile_str(php).unwrap(), php);
}
