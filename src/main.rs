use std::{
    fs::{read_dir, read_to_string},
    path::{Path, PathBuf},
};

static mut FORCE: bool = false;

fn compile_file(from: impl AsRef<Path>, to: impl AsRef<Path>) {
    let inp = read_to_string(from.as_ref()).expect("failed to read file");
    let out = aphp::compile_str(&inp).expect("compilation failiure");

    if to.as_ref().exists() && !unsafe { FORCE } {
        eprintln!("Won't overwrite. Use force")
    } else {
        std::fs::write(to, out).expect("can't write to target.");
    }
}

fn compile_dir(path: impl AsRef<Path>) {
    for entry in read_dir(path).unwrap().filter_map(|e| e.ok()) {
        if entry.file_type().unwrap().is_dir() {
            compile_dir(entry.path());
        } else {
            if entry.path().extension().and_then(|s| s.to_str()) == Some("aphp") {
                let mut to = entry.path().clone();
                to.set_extension("php");
                compile_file(entry.path(), to);
            }
        }
    }
}

fn main() {
    let mut args = std::env::args();
    args.next();
    let mut arg = args.next().unwrap_or_default();

    if ["f", "force"].contains(&arg.as_str()) {
        unsafe { FORCE = true } // This is fine, the program isn't mulitthreaded
        arg = args.next().unwrap_or_default();
    }

    match arg.as_str() {
        "" => {
            compile_dir(".");
        }
        "c" | "compile" => {
            let from = args.next().expect("Supply a file to compile");
            let to = args.next().expect("Supply a location");
            compile_file(from, to);
        }
        s => {
            let path = PathBuf::from(s);

            if path.is_file() {
                let content = read_to_string(s).unwrap();
                let out = aphp::compile_str(&content).unwrap();
                print!("{}", out);
            } else if path.is_dir() {
                compile_dir(path)
            }
        }
    }

    unsafe { drop(FORCE) } // Just in case
}
