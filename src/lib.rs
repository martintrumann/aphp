use logos::{Lexer, Logos, Span};
use std::fmt::Write;

#[derive(Debug, PartialEq, Eq)]
enum KeywordType {
    Named,
    Initializing,
    Normal,
}

fn is_keyword(s: &str) -> Option<KeywordType> {
    if ["function", "class", "const"].contains(&s) {
        return Some(KeywordType::Named);
    }

    if ["private", "public", "static"].contains(&s) {
        return Some(KeywordType::Initializing);
    }

    [
        "fn",
        "new",
        // types
        "void",
        "bool",
        "int",
        "float",
        "string",
        "array",
        "object",
        "callable",
        "iterable",
        "null",
        "NULL",
        // output
        "echo",
        "print",
        // flow control
        "do",
        "while",
        "for",
        "foreach",
        "as",
        "if",
        "else",
        "elseif",
        "switch",
        "match",
        //
        "continue",
        "break",
        "return",
        //
        "use",
        "global",
        //
        "require",
        "include",
        "require_once",
        "include_once",
    ]
    .contains(&s)
    .then_some(KeywordType::Normal)
}

fn quoted(l: &mut Lexer<Token>, quote: char) -> bool {
    let mut esc = false;
    for (i, ch) in l.remainder().chars().enumerate() {
        if esc {
            esc = false
        } else if ch == '\\' {
            esc = true
        } else if ch == quote {
            l.bump(i + 1);
            return true;
        }
    }
    false
}

fn double_qouted(l: &mut Lexer<Token>) -> bool {
    quoted(l, '"')
}

fn single_qouted(l: &mut Lexer<Token>) -> bool {
    quoted(l, '\'')
}

fn till_newline(l: &mut Lexer<Token>) {
    let n = l
        .remainder()
        .find(['\n', '\r'])
        .unwrap_or_else(|| l.remainder().len());

    l.bump(n)
}

fn till_comment_end(l: &mut Lexer<Token>) -> bool {
    l.remainder().find("*/").map(|n| l.bump(n + 2)).is_some()
}

#[derive(Logos, Debug, PartialEq)]
pub enum Token {
    #[token("let ")]
    Let,

    #[token("->")]
    Arrow,

    #[token("=>")]
    BigArrow,

    #[regex("<\\?(php|=)?")]
    Begin,
    #[regex("\\?>")]
    End,

    #[regex("[a-zA-Z0-9_]+\\(")]
    Function,
    #[token("): ")]
    FunctionType,

    #[regex("\\$[a-zA-Z0-9_]+")]
    PhpVar,

    #[regex("[a-zA-Z_]+:", priority = 10)]
    NamedArg,

    #[regex("[A-Z_]+", priority = 2)]
    Constant,

    #[regex("_[A-Z_]+", priority = 3)]
    #[regex("[a-zA-Z0-9_]+")]
    #[token("GLOBALS")]
    Text,

    #[token("\\")]
    Backslash,

    #[token("?")]
    Questonmark,

    #[token(":")]
    Colon,
    #[token(";")]
    SemiColon,
    #[token(",")]
    Comma,

    #[token("{")]
    ScopeStart,
    #[token("}")]
    ScopeEnd,

    #[token("(")]
    #[token("[")]
    GroupStart,
    #[token(")")]
    #[token("]")]
    GroupEnd,

    #[token("++")]
    Incrementor,
    #[token("--")]
    Decrementor,

    #[regex("\"", double_qouted)]
    #[regex("\'", single_qouted)]
    #[regex("[0-9]+", priority = 2)]
    #[token("true")]
    #[token("false")]
    Literal,

    #[regex("//.*", till_newline)]
    LineComment,

    #[regex(r"/\*", till_comment_end)]
    RangeComment,

    #[regex(r"[ \t\n\f]+")]
    Whitespace,

    #[token("=")]
    Assigment,

    #[token(".")]
    #[token("+")]
    #[token("-")]
    #[token("/")]
    #[token("*")]
    #[token("<")]
    #[token("<=")]
    #[token(">")]
    #[token(">=")]
    #[token("!")]
    #[token("!=")]
    #[token("||")]
    #[token("&&")]
    #[token("==")]
    #[token("===")]
    Operator,

    Keyword,
    Name,
    Variable,

    #[error]
    Error,
}

#[derive(Debug)]
pub struct Error<'a> {
    loc: Span,
    text: &'a str,
}

impl std::fmt::Display for Error<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} at {}..{}", self.text, self.loc.start, self.loc.end)
    }
}

fn variable_check<'a>(var: &'a str, vars: &mut Vars<'a>, php: bool) {
    if !(vars.contains(&var)
        || ["this", "argv", "argc", "http_response_header"].contains(&var)
        || var.len() == 1
        || var.starts_with("_")
        || var
            .chars()
            .all(|ch| !ch.is_ascii_alphabetic() || ch.is_ascii_uppercase()))
    {
        eprintln!(
            "Uninitialized variable {}{}",
            php.then_some("$").unwrap_or_default(),
            var
        );
        vars.push(&var);
    }
}

struct Vars<'a>(Vec<Vec<&'a str>>);

impl<'a> Vars<'a> {
    fn new() -> Self {
        Self(vec![Vec::new()])
    }

    fn push(&mut self, s: &'a str) {
        self.0.last_mut().unwrap().push(s)
    }

    fn contains(&self, s: &str) -> bool {
        for scope in self.0.iter() {
            if scope.contains(&s) {
                return true;
            }
        }
        false
    }

    fn start_scope(&mut self) {
        self.0.push(Vec::new())
    }

    fn end_scope(&mut self) {
        self.0.pop();
    }
}

pub fn compile_str(s: &str) -> Result<String, Error> {
    let mut out = String::new();
    let mut lex = Token::lexer(s);
    let mut vars = Vars::new();

    let mut on = false;

    let mut last_typ = None;
    while let Some(mut i) = lex.next() {
        if i == Token::Begin {
            on = true;
        } else if i == Token::End {
            on = false;
        }

        if !on {
            out.push_str(lex.slice());
            continue;
        }

        if i == Token::Text {
            i = if let Some(typ) = is_keyword(lex.slice()) {
                last_typ = Some(typ);
                Token::Keyword
            } else if last_typ == Some(KeywordType::Named) {
                last_typ = None;
                Token::Name
            } else if last_typ == Some(KeywordType::Initializing) {
                last_typ = None;
                vars.push(lex.slice());
                Token::Variable
            } else {
                variable_check(lex.slice(), &mut vars, false);
                Token::Variable
            };
        }

        if i == Token::ScopeEnd {
            vars.start_scope();
        }

        if i == Token::ScopeEnd {
            vars.end_scope();
        }

        match i {
            Token::Text => unreachable!(),
            Token::Let => {
                if lex.next() == Some(Token::Text) {
                    if is_keyword(lex.slice()).is_some() {
                        return Err(Error {
                            text: "Can't have a keyword as a variable.",
                            loc: lex.span(),
                        });
                    }
                    vars.push(lex.slice());
                    write!(out, "${}", lex.slice()).unwrap();
                    continue;
                } else {
                    return Err(Error {
                        text: "'let' must have a variable.",
                        loc: lex.span(),
                    });
                }
            }
            Token::Arrow | Token::Backslash | Token::FunctionType => {
                last_typ = Some(KeywordType::Named);
            }
            Token::Variable => {
                write!(out, "${}", lex.slice()).unwrap();
                continue;
            }
            Token::PhpVar => {
                let name = &lex.slice()[1..];
                if !vars.contains(&name) {
                    variable_check(name, &mut vars, true);
                }
            }
            Token::Error => {
                eprintln!("parser_error: {:?} {:?}", lex.span(), lex.slice());
                // return Err(Error {
                // 	text: lex.slice(),
                // 	loc: lex.span(),
                // })
            }
            _ => (),
        }

        out.push_str(lex.slice());
    }

    Ok(out)
}
